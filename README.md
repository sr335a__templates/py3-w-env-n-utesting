# Py3-w-Env-n-uTesting
A simple Py project template with simple doctest unitTesting!

> How to clone and set env and open VSCode?
```sh
git clone https://gitlab.com/sr335a__templates/py3-w-env-n-utesting.git my_new_py_project
cd my_new_py_project
python3 -m venv _env
```
> In Windows
```sh
_env/Scripts/activate
code .
```
> In Linux / Linux subsystem for Windows
```sh
source _env/bin/activate
code .
```
The prompt should now be sucfixed with `(_env)` indicating that you are in the `virtual-env` for this code-base and all `pip install` is isaolated.

> Installing the requirements 
```sh
pip install -f requirements.txt
```

> Exploring the `Example-Py-Code-w-Simple-uTesting.py` and unit-testing!
```sh
python Example-Py-Code-w-Simple-uTesting.py -v
```

> Check these steps before to push the code back; to avoid any `lose`!
```sh
pip freeze > requirements.txt
git add --all
git commit -m "<< Provide the appropriate commit message here >>"
git push
```

> Close the working env
```
deactivate 
```

Note: The _env environment folder is added to the ,gitignore file.

## How to clone the template project and commit to git as my_new_py_project

> Create the `my_new_py_project` in the gitlab or github as a blank project so that the remote URL is available; 
> - DO NOT even add the default README.md ( ensure master branch is not created)
> - (for example lets say the url is `https://new-repo-url-for__my_new_py_project.git`)
> Changing the remote origin
```sh 
cd my_new_py_project
git remote remove origin
git remote add origin https://new-repo-url-for__my_new_py_project.git
git fetch 
git status 
git add --all
git push --set-upstream origin master
```
> - In the event the master got created accidently;
```sh
cd my_new_py_project
git remote remove origin
git remote add origin https://new-repo-url-for__my_new_py_project.git
git fetch 
git status 
git add --all
git checkout -b initial_commit
git push --all
```
> - - Once code is pushed 
> - - - Create a merge request to merge `initial_commit` into `master`
